import {AsyncContainerModule, interfaces} from "inversify";
import * as mongoose from "mongoose";
import {MongoMemoryServer} from 'mongodb-memory-server';
import {Connection} from "../../../src/definitions";
import {SHARED_TYPES} from "../../../src/ioc/types";

export const mongooseMockModule = new AsyncContainerModule(async (bind: interfaces.Bind,
                                                                         unbind: interfaces.Unbind,
                                                                         isBound: interfaces.IsBound) => {
    if(!isBound(mongoose.Mongoose)) {
        const mongod = new MongoMemoryServer();
        const mongoDBURI = await mongod.getUri();
        const connection = await mongoose.connect(mongoDBURI, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        bind<Connection>(SHARED_TYPES.Connection).toDynamicValue((ctx) => {
            return {
                close(callback: (err?: Error) => void): any {
                    mongod.stop()
                        .then((res) => callback(undefined))
                        .catch(callback);
                }
            }
        });
        bind<Connection>(SHARED_TYPES.Connection).toConstantValue(connection.connection);
        bind<mongoose.Mongoose>(mongoose.Mongoose).toConstantValue(connection);
    }
});