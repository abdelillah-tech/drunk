import {expect} from 'chai';
import {Container} from "inversify";
import {appModule} from "../../../src/platforms/app";
import {mongooseModule} from "../../../src/platforms/mongoose";
import {mongooseMockModule} from "../mongoose/mongoose-mock.module";
import {SHARED_TYPES} from "../../../src/ioc/types";
import {closeConnections, Connection} from "../../../src/definitions";
import * as http from "http";
import {SuperTest, Test} from "supertest";
import * as supertest from "supertest";

const container = new Container();

describe('Cocktail Controller', () => {

    let request: SuperTest<Test>;

    beforeAll(async () => {
        container.load(appModule);
        await container.loadAsync(mongooseMockModule);
        await container.loadAsync(mongooseModule);
        const server = container.get<http.Server>(SHARED_TYPES.Server);
        request = supertest(server);
    });

    afterAll(async () => {
        const connections = container.getAll<Connection>(SHARED_TYPES.Connection);
        await closeConnections(connections);
    });

    it('should insert cocktail', async () => {
        const res = await request.post('/cocktail')
                                 .send({
                                    'name': 'monako',
                                    'price': 6,
                                    'alcool': 14,
                                    'ingredients' : 'beer | ciroc',
                                    'description' : 'couleur rouge'
                                 })
                                .set('Content-Type', 'application/json');
        const data = res.body;
        expect(res.status).to.be.eq(200);
        expect(data).to.be.instanceOf(Object);
    });

    it('should raise a 400 error', async () => {
        const res = await request.post('/cocktail')
            .send({
                name: 'Intenso'
            })
            .set('Content-Type', 'application/json');
        expect(res.status).to.be.eq(400);
    });

});