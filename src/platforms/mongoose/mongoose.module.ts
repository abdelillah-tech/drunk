import {AsyncContainerModule, interfaces} from "inversify";
import * as mongoose from "mongoose";
import {SHARED_TYPES} from "../../ioc/types";
import {ICocktailDAO} from "../../definitions/dao";
import {CocktailMongooseDAO} from "./dao";
import {Connection} from "../../definitions";

export const mongooseModule = new AsyncContainerModule(async (bind: interfaces.Bind,
                                                                     unbind: interfaces.Unbind,
                                                                     isBound: interfaces.IsBound) => {
    if(!isBound(mongoose.Mongoose)) {
        const connection = await mongoose.connect(process.env.MONGODB_URI as string, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            auth: {
                user: process.env.MONGODB_USER as string,
                password: process.env.MONGODB_PASSWORD as string
            },
            authSource: 'admin'
        });
        bind<Connection>(SHARED_TYPES.Connection).toConstantValue(connection.connection);
        bind<mongoose.Mongoose>(mongoose.Mongoose).toConstantValue(connection);
    }
    bind<ICocktailDAO>(SHARED_TYPES.DAO.Cocktail).to(CocktailMongooseDAO).inSingletonScope();
});