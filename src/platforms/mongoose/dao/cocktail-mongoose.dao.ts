import {BaseMongooseDAO} from "./base-mongoose.dao";
import {CocktailModel, ICocktailDocument} from "../models/cocktail.model";
import {Model, Mongoose} from "mongoose";
import {ICocktailDAO} from "../../../definitions/dao";
import {ICocktail} from "../../../definitions/models";
import {inject, injectable} from "inversify";

@injectable()
export class CocktailMongooseDAO extends BaseMongooseDAO<ICocktailDocument> implements ICocktailDAO {

    public constructor(@inject(Mongoose) mongoose: Mongoose) {
        super(mongoose);
    }

    getModel(): Model<ICocktailDocument> {
        return CocktailModel;
    }

    save(cocktail: ICocktail): Promise<ICocktailDocument> {
        return this.getModel().create(cocktail);
    }

    searchByName(name: string): Promise<ICocktailDocument[]> {
        return this.getModel().find({
            name: {
                $regex: new RegExp(name),
                $options: "i"
            }
        }).exec();
    }
}