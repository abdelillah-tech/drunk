import {IBaseDAO} from "../../../definitions/dao";
import {Document, Model, Mongoose, Types} from "mongoose";
import {inject, injectable} from "inversify";
import {Id} from "../../../definitions/models";

@injectable()
export abstract class BaseMongooseDAO<Type extends Document> implements IBaseDAO<Type> {

    readonly mongoose: Mongoose;

    protected constructor(@inject(Mongoose) mongoose: Mongoose) {
        this.mongoose = mongoose;
    }

    abstract getModel(): Model<Type>;

    getAll(): Promise<Type[]> {
        return this.getModel().find().exec(); // retourne l'ensemble de la collection
    }

    getById(id: Id): Promise<Type | null> {
        if(!Types.ObjectId.isValid(id.toString())) {
            return Promise.resolve(null);
        }
        return this.getModel().findById(id).exec();
    }

    async removeById(id: Id): Promise<boolean> {
        if(!Types.ObjectId.isValid(id.toString())) {
            return false;
        }
        const obj = await this.getModel().findByIdAndDelete(id);
        return obj !== null;
    }
}