import {ICocktail} from "../../../definitions/models";
import {Document, Schema} from "mongoose";
import * as mongoose from "mongoose";

export type ICocktailDocument = ICocktail & Document;

export const CocktailSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    alcool: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    ingredients: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
}, {
    versionKey: false,
    timestamps: true
});

export const CocktailModel = mongoose.model<ICocktailDocument>('Cocktail', CocktailSchema);