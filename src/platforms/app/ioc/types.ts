export const APP_TYPES = {
    RootController: Symbol.for('RootController'),
    Controllers: {
        Cocktail: Symbol.for('CocktailController')
    }
};