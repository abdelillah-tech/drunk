import * as Router from "koa-router";
import {Context} from "koa";
import {inject, injectable} from "inversify";
import {SHARED_TYPES} from "../../../ioc/types";
import {ICocktailDAO} from "../../../definitions/dao";
import {plainToClass} from "class-transformer";
import {CocktailDTO} from "../../../definitions/dto";
import {validate} from "class-validator";

@injectable()
export class CocktailController {

    readonly cocktailDAO: ICocktailDAO;

    constructor(@inject(SHARED_TYPES.DAO.Cocktail) cocktailDAO: ICocktailDAO) {
        this.cocktailDAO = cocktailDAO;
    }

    // Context -> Context KOA (request http, response, etc..)
    async getAll(ctx: Context) {
        if (ctx.query.name) {
            ctx.body = await this.cocktailDAO.searchByName(ctx.query.name);
            return;
        }
        ctx.body = await this.cocktailDAO.getAll();
    }

    async create(ctx: Context) {
        const data = ctx.request.body;
        const cocktail = plainToClass(CocktailDTO, data);
        const errors = await validate(cocktail);
        if(errors.length > 0) {
            ctx.throw(400); // Permet de finir la requete et de retourner un status HTTP 400 (Bad request)
        }
        ctx.body = await this.cocktailDAO.save(cocktail);
    }

    async delete(ctx: Context) {
        const id = ctx.params.id;
        try {
            await this.cocktailDAO.removeById(id);
            ctx.body = 'Deleted';
        } catch (err) {
            ctx.throw(err);
        }
    }

    build(): Router {
        const router = new Router({
            prefix: '/cocktail'
        });
        router.get('/', this.getAll.bind(this))
        router.post('/', this.create.bind(this));
        router.delete('/:id', this.delete.bind(this));
        return router;
    }
}