import {ContainerModule, interfaces} from "inversify";
import * as http from "http";
import * as Koa from "koa";
import {SHARED_TYPES} from "../../ioc/types";
import {CocktailController} from "./controllers";
import {APP_TYPES} from "./ioc/types";
import * as bodyParser from "koa-bodyparser";
import {Connection} from "../../definitions";

export const appModule = new ContainerModule((bind: interfaces.Bind) => {
   bindRootController(bind, APP_TYPES.Controllers.Cocktail, CocktailController);
   bind<http.Server>(SHARED_TYPES.Server).toDynamicValue((context: interfaces.Context) => {
       const app = new Koa();
       app.use(bodyParser());
       const rootControllers: any[] = context.container.getAll(APP_TYPES.RootController);
       for(const controller of rootControllers) {
           const router = controller.build();
           app.use(router.routes());
           app.use(router.allowedMethods());
       }
       const port = process.env.PORT || 3000;
       const server = app.listen(port, () => console.log(`API Listening on ${port}...`));
       bind<Connection>(SHARED_TYPES.Connection).toConstantValue(server);
       return server;
   }).inSingletonScope();
});

function bindRootController(bind: interfaces.Bind, symbol: symbol, controller: any): void {
    bind(symbol).to(controller).inSingletonScope();
    bind(APP_TYPES.RootController).toDynamicValue((context: interfaces.Context) => {
        return context.container.get(symbol);
    });
}