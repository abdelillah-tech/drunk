export const SHARED_TYPES = {
    Server: Symbol.for('Server'),
    Connection: Symbol.for('Connection'),
    DAO: {
        Cocktail: Symbol.for('CocktailDAO')
    }
};