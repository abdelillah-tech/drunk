module.exports = {
    globals: {
        'ts-jest': {
            tsconfig: './test/tsconfig.json'
        }
    },
    preset: 'ts-jest',
    testEnvironment: 'node',
    collectCoverage: true,
    setupFiles: [
        './test/load-env.ts'
    ]
}
