import {Id} from "./id.interface";

export interface ICocktail {
    id?: Id;
    name: string;
    alcool: number;
    price: number;
}