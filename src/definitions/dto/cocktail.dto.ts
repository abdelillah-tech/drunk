import {IsNumber, IsString, Max, Min, MinLength} from "class-validator";
import {ICocktail} from "../models";

export class CocktailDTO implements ICocktail {

    @IsString()
    @MinLength(1)
    readonly name!: string;

    @IsNumber()
    @Min(0)
    readonly price!: number;

    @IsNumber()
    @Min(0)
    @Max(100)
    readonly alcool!: number;

    @IsString()
    @MinLength(4)
    readonly ingredients!: string;

    @IsString()
    @MinLength(4)
    readonly description!: string;
}