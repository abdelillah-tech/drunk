import {ICocktail} from "../models";
import {IBaseDAO} from "./base.dao.interface";

export interface ICocktailDAO extends IBaseDAO<ICocktail> {
    save(cocktail: ICocktail): Promise<ICocktail>;
    searchByName(name: string): Promise<ICocktail[]>
}