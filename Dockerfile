FROM node:latest as tsc-builder
WORKDIR /workspace
COPY . /workspace/
RUN npm i && npx tsc

FROM node:latest
WORKDIR /workspace
COPY package*.json /workspace/
RUN npm i --production
COPY --from=tsc-builder /workspace/dist /workspace/dist
EXPOSE $PORT
CMD npm start
